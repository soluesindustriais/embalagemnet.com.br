<div class="logo-top">
  <a href="<?= $url ?>" title="Início">
    <img style="width: 100px; height: 100px; object-fit: contain;" src="imagens/img-home/logo.png" alt="Logo" title="Logo"></a>
</div>
<ul>
  <li><a class="nav-link nav-link-ltr" href="<?= $url ?>" title="Página inicial"><span class="fas-icons"><i class="fas fa-home"></i></span>Início</a></li>
  <li><a class="nav-link nav-link-ltr" href="<?= $url ?>sobre-nos"><span class="fas-icons"><i class="fas fa-user"></i></span>Sobre Nós</a></li>
  <li class="dropdown"><a href="<?= $url ?>produtos-categoria" title="Produtos"><span class="fas-icons"><i class="fas fa-box-open"></i></span>Produtos</a>
    <ul class="sub-menu">
      <? include('inc/sub-menu.php'); ?>
    </ul>
  </li>
  <li><a class="nav-link nav-link-ltr" href="<?= $url ?>blog"><span class="fas-icons"><i class="fas fa-book"></i></span>Blog</a></li>

  
  <li  class="buy-button">
    <a title="logo" class="nav-link nav-link-ltr" rel="noopener noreferrer" href="https://www.solucoesindustriais.com.br/faca-parte" target="_blank" class="btn btn-primary">Gostaria de
      anunciar?</a>
  </li>

  <!--<li class="dropdown"><a href="<?= $url ?>informacoes" title="Informações"><span class="fas-icons"><i
          class="fas fa-info-circle"></i></span>Informações <span class="fas-icons-down"><i
          class="fa fa-angle-down"></i></span></a>
    <ul class="sub-menu sub-menu-mpi">
      <? include('inc/sub-menu-info.php'); ?>
    </ul>
  </li>-->