<?
$nomeSite			= 'Portal das Embalagens';
$slogan				= 'Simplesmente o melhor do ramo!';

$dir = $_SERVER['SCRIPT_NAME'];
$dir = pathinfo($dir);
$host = $_SERVER['HTTP_HOST'];
$http = $_SERVER['REQUEST_SCHEME'];
if ($dir["dirname"] == "/") { $url = $http."://".$host."/"; }
else { $url = $http."://".$host.$dir["dirname"]."/";  }


$emailContato		= 'everton.lima@solucoesindustriais.com.br';
$rua				= 'Rua Pequetita, 179';
$bairro				= 'Vila Olimpia';
$cidade				= 'São Paulo';
$UF					= 'SP';
$cep				= 'CEP: 04552-060';
$latitude			= '-22.546052';
$longitude			= '-48.635514';
$idAnalytics		= 'UA-119965399-10';
$senhaEmailEnvia	= '102030'; // colocar senha do e-mail mkt@dominiodocliente.com.br
$explode			= explode("/", $_SERVER['PHP_SELF']);
$urlPagina 			= end($explode);
$urlPagina	 		= str_replace('.php','',$urlPagina);
$urlPagina 			== "index"? $urlPagina= "" : "";
$urlAnalytics = str_replace("http://www.", '', $url);
$urlAnalytics = str_replace("/", '', $urlAnalytics);
//reCaptcha do Google
$siteKey = '6Lfc7g8UAAAAAHlnefz4zF82BexhvMJxhzifPirv';
$secretKey = '6Lfc7g8UAAAAAKi8al32HjrmsdwoFoG7eujNOwBI';


//Breadcrumbs
include('inc/categoriasGeral.php');
$caminho ='
	<div class="breadcrumb" id="breadcrumb" style="margin-top: 50px">
		<div class="wrapper">
			<div class="bread__row">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
		<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
		  <a href="'.$url.'" itemprop="item" title="Home">
			<span itemprop="name"> Home  </span>
		  </a>
		  <meta itemprop="position" content="1" />
		</li>   
		<li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
		  <span itemprop="name">'.$h1.'</span>
		  <meta itemprop="position" content="2" />
		</li>
	  </ol>
	</nav>
	</div>
	</div>
	</div>
	  ';

$caminho2	 = '<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >';
$caminho2	.= '<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title">';
$caminho2	.= '<i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »';
$caminho2	.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminho2	.= '<a href="'.$url.'produtos-categoria" title="Produtos" class="category" itemprop="url">';
$caminho2	.= '<span itemprop="title"> Produtos </span></a> »';
$caminho2	.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminho2	.= '<strong><span class="page" itemprop="title" > '.$h1.'</span></strong></div></div></div>';

$caminhoembalagem_a_vacuo_e_encolhivel = '<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >';
$caminhoembalagem_a_vacuo_e_encolhivel.= '<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title">';
$caminhoembalagem_a_vacuo_e_encolhivel.= '<i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »';
$caminhoembalagem_a_vacuo_e_encolhivel.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhoembalagem_a_vacuo_e_encolhivel.= '<a href="'.$url.'produtos-categoria" title="Produtos" class="category" itemprop="url">';
$caminhoembalagem_a_vacuo_e_encolhivel.= '<span itemprop="title"> Produtos </span></a> »';
$caminhoembalagem_a_vacuo_e_encolhivel.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhoembalagem_a_vacuo_e_encolhivel.= '<a href="'.$url.'embalagem-a-vacuo-e-encolhivel-categoria" title="Categoria - Embalagem à Vácuo e Encolhível" class="category" itemprop="url">';
$caminhoembalagem_a_vacuo_e_encolhivel.= '<span itemprop="title"> Categoria - Embalagem à Vácuo e Encolhível </span></a> »';
$caminhoembalagem_a_vacuo_e_encolhivel.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhoembalagem_a_vacuo_e_encolhivel.= '<strong><span class="page" itemprop="title" > '.$h1.'</span></strong></div></div></div></div>';

$caminhoembalagem_filme  = '<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >';
$caminhoembalagem_filme .= '<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title">';
$caminhoembalagem_filme .= '<i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »';
$caminhoembalagem_filme .= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhoembalagem_filme .= '<a href="'.$url.'produtos-categoria" title="Produtos" class="category" itemprop="url">';
$caminhoembalagem_filme .= '<span itemprop="title"> Produtos </span></a> »';
$caminhoembalagem_filme .= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhoembalagem_filme .= '<a href="'.$url.'embalagem-filme-categoria" title="Categoria - Embalagem Filme " class="category" itemprop="url">';
$caminhoembalagem_filme .= '<span itemprop="title"> Categoria - Embalagem Filme </span></a> »';
$caminhoembalagem_filme .= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhoembalagem_filme .= '<strong><span class="page" itemprop="title" > '.$h1.'</span></strong></div></div></div></div>';
$caminho2 ='
	<div class="breadcrumb" id="breadcrumb" style="margin-top: 50px">
		<div class="wrapper">
			<div class="bread__row">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
		<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
		  <a href="'.$url.'" itemprop="item" title="Home">
			<span itemprop="name"> Home  </span>
		  </a>
		  <meta itemprop="position" content="1" />
		</li>   
		<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="'.$url.'produtos" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos  </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
		<li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
		  <span itemprop="name">'.$h1.'</span>
		  <meta itemprop="position" content="3" />
		</li>
	  </ol>
	</nav>
	</div>
	</div>
	</div>
	';

$caminhoembalagem_a_vacuo_e_encolhivel='
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="'.$url.'" itemprop="item" title="Home">
                        <span itemprop="name"> Home  </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="'.$url.'produtos" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos  </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="'.$url.'embalagem-a-vacuo-e-encolhivel-categoria" itemprop="item" title="Embalagem-a-vacuo-e-encolhivel - Categoria">
                      <span itemprop="name">Embalagem a vacuo e encolhivel - Categoria  </span>
                    </a>
                    <meta itemprop="position" content="3" />
                  </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">'.$h1.'</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';

$caminhoembalagem_filme='
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="'.$url.'" itemprop="item" title="Home">
                        <span itemprop="name"> Home  </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="'.$url.'produtos" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos  </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="'.$url.'embalagem-filme-categoria" itemprop="item" title="Embalagem-filme - Categoria">
                      <span itemprop="name">Embalagem filme - Categoria  </span>
                    </a>
                    <meta itemprop="position" content="3" />
                  </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">'.$h1.'</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';


    


?>