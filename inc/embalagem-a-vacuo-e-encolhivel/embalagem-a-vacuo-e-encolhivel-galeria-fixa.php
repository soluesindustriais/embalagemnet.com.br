
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>
<?php $lista = array('Saco a vácuo','Embalagem a vácuo','Saco para embalar a vácuo','Embalagem a vácuo para alimentos','Saco a vácuo para alimentos','Saco a vácuo com bombinha','Plástico termo retrátil','Embalagem termoencolhível','Embalagem a vácuo com ranhuras','Plástico termoencolhível','Embalagem a vácuo personalizada','Plástico para embalar a vácuo','Saco plástico para embalagem a vácuo','Embalagens a vácuo onde comprar','Sacos para congelar alimentos a vácuo','Saquinhos com ranhuras para seladora a vácuo','Saco plástico a vácuo para alimentos','Embalagem termo retrátil','Saco a vacuo para alimentos com bombinha','Saco plástico termo encolhível','Embalagem termoencolhível para queijo','Plastico para maquina a vácuo','Plástico termo retrátil embalagem','Embalagem termoencolhível para carne','Embalagem a vácuo para queijo','Embalagem a vácuo de alimentos sp','Saco plástico para embalar alimentos a vácuo','Embalagem a vácuo para alimentos preço','Saco laminado para embalagem a vácuo','Sacos para embalagens a vácuo','Saco termo encolhível','Plástico termo contrátil','Poliolefínico encolhível','Bobina polietileno termo encolhível','Bobina termo retrátil'); shuffle($lista); for($i=1;$i<13;$i++){ ?>  
<?php
$folder = "embalagem-a-vacuo-e-encolhivel";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/embalagem-a-vacuo-e-encolhivel/embalagem-a-vacuo-e-encolhivel-" . $i . ".jpg",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-03-27"
];
}
?>

<ul class="thumbnails-mod17">
<?php for ($i = 1; $i < 13; $i++) { ?>
    <li>
        <a class="lightbox" href="<?= $url; ?>imagens/embalagem-a-vacuo-e-encolhivel/embalagem-a-vacuo-e-encolhivel-<?= $i ?>.jpg" title="<?= $lista[$i] ?>">
            <img src="<?= $url; ?>imagens/embalagem-a-vacuo-e-encolhivel/thumbs/embalagem-a-vacuo-e-encolhivel-<?= $i ?>.jpg" class="lazyload" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
        </a>
    </li>
<?php } ?>
</ul>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>