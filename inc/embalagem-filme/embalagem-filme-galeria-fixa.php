
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>
<?php $lista = array('Filme stretch','Filme stretch manual','Filme de polietileno','Bobina stretch','Plástico filme stretch','Filme termo encolhível','Fabricante de filme stretch','Filme poliolefínico','Filme shrink','Filme stretch para paletização','Filme stretch preço','Bobina de filme stretch','Filme stretch colorido','Filme stretch reciclado','Filme termo encolhível shrink','Filme encolhível','Embalagem filme stretch','Filme poliolefínico termoencolhível','Fornecedores de filme stretch','Filme plástico stretch','Filme stretch para paletização preço','Filme contratil','Filme plástico de polietileno','Filme stretch para embalagem','Bobina stretch preço','Empresa de filme stretch','Filme polietileno gofrado','Filme pvc termo encolhível preço','Filme stretch bobina jumbo','Filme stretch manual 500mm','Filme termo encolhível preço','Filme termo encolhível pvc','Filme stretch com aplicador','Filme poliolefínico encolhível','Bobina de filme termo encolhível','Bobina filme stretch a venda','Comprar filme poliolefínico','Distribuidores de filme stretch','Empresa de filme poliolefínico','Empresa fabricante de filme stretch','Fábrica de filme stretch','Fabricante de filme poliolefínico','Filme de polietileno liso ou impresso','Filme pebd termo-encolhível','Filme polietileno canela','Filme polietileno cristal','Filme polietileno recuperado','Filme polietileno shrink','Filme poliolefínico de alta resistência','Filme poliolefínico impresso','Filme poliolefínico para indústria alimentícia','Filme poliolefínico para indústrias','Filme poliolefínico preço','Filme poliolefínico shrink','Filme stretch industrial','Filme stretch manual comprar','Filme stretch para paletização onde comprar','Filme termo encolhível reciclado','Fornecedor de filme poliolefínico','Onde comprar filme poliolefínico','Venda de filme poliolefínico','Bobina de plástico stretch'); shuffle($lista); for($i=1;$i<13;$i++){ ?>  
<?php
$folder = "embalagem-filme";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/embalagem-filme/embalagem-filme-" . $i . ".jpg",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-03-27"
];
}
?>

<ul class="thumbnails-mod17">
<?php for ($i = 1; $i < 13; $i++) { ?>
    <li>
        <a class="lightbox" href="<?= $url; ?>imagens/embalagem-filme/embalagem-filme-<?= $i ?>.jpg" title="<?= $lista[$i] ?>">
            <img src="<?= $url; ?>imagens/embalagem-filme/thumbs/embalagem-filme-<?= $i ?>.jpg" class="lazyload" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
        </a>
    </li>
<?php } ?>
</ul>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>