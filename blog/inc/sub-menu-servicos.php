<?php include_once('inc/vetServicos.php');
foreach ($vetServicos as $key => $item) :
    if ($item['sub-menu'] === null) : ?>
        <li><a href="<?= (strpos($item['url'], 'http') !== false ? $item['url'] : $url . $item['url']) ?>" title="<?= $item['title'] ?>" <?=(strpos($item['url'], 'http') !== false ? 'target="_blank" rel="nofollow"' : "")?> <?= (strpos($item['url'], '.pdf') !== false ? 'target="_blank"' : "")?>><?= $item['title'] ?></a></li>
    <?php else : ?>
        <li class="dropdown"><a href="<?= $url . $item['url'] ?>" title="<?= $item['title'] ?>"><?= $item['title'] ?></a>
            <ul class="sub-menu">
                <? include("inc/" . $item['sub-menu'] . ".php"); ?>
            </ul>
        </li>
<?php endif;
endforeach;
?>