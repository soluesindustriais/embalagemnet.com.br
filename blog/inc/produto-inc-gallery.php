<!-- DEFAULT GALLERY -->
<?php if (GALLERY_DEFAULT): ?>
  <div class="prod-inc-cover">
    <a href="<?= RAIZ . '/doutor/uploads/' . $prod_cover; ?>" title="<?= $prod_title; ?>" data-fancybox="group1" data-caption="<?=$prod_title?>" class="lightbox">
      <?= Check::Image('doutor/uploads/' . $prod_cover, $prod_title, NULL, 380, 380); ?>
    </a>
  </div>
  <?php
  $Read->ExeRead(TB_GALLERY, "WHERE gallery_rel = :id AND user_empresa = :emp AND cat_parent IN(:cat)", "id={$prod_id}&emp=" . EMPRESA_CLIENTE . "&cat={$cat_parent}");
  if ($Read->getResult()): ?>
    <div class="prod-inc-default-gallery">
      <h2>Confira mais imagens</h2>
      <div class="row">
        <?php
        foreach ($Read->getResult() as $gallery):
          extract($gallery); ?>
          <div class="p-2 col-3 col-sm-3 col-xs-6">
            <div class="gallery__item">
              <a href="<?= RAIZ . '/doutor/uploads/' . $gallery_file; ?>" title="<?= $prod_title; ?>" data-fancybox="group1" data-caption="<?=$prod_title?>" class="lightbox">
                <?= Check::Image('doutor/uploads/' . $gallery_file, $prod_title, NULL, 100, 100); ?>
              </a>
            </div>
          </div>
          <?php
        endforeach; ?>
      </div>
    </div>
  <? endif; ?>
  <!-- END DEFAULT GALLERY -->
<?php elseif (GALLERY_CUSTOM): ?>
  <!-- CUSTOM GALLERY -->
  <div class="prod-inc-custom-gallery">
    <?php $Read->ExeRead(TB_GALLERY, "WHERE gallery_rel = :id AND user_empresa = :emp AND cat_parent IN(:cat)", "id={$prod_id}&emp=" . EMPRESA_CLIENTE . "&cat={$cat_parent}");
    if (!$Read->getResult()): ?>
     <div class="prod-inc-cover">
      <a href="<?= RAIZ . '/doutor/uploads/' . $prod_cover; ?>" title="<?= $prod_title; ?>" data-fancybox="group1" data-caption="<?=$prod_title?>" class="lightbox">
        <?= Check::Image('doutor/uploads/' . $prod_cover, $prod_title, NULL, 380, 380); ?>
      </a>
    </div>
    <?php
  else: ?>
    <?php $nImg = count($Read->getResult()) + 1;?>
    <div class="gallery__main" data-slick='{"arrows": <?=($nImg <= "3") ? "false" : "true" ?>}'>
      <a href="<?= RAIZ . '/doutor/uploads/' . $prod_cover; ?>" title="<?= $prod_title; ?>" data-fancybox="group1" data-caption="<?=$prod_title?>" class="lightbox">
        <?= Check::Image('doutor/uploads/' . $prod_cover, $prod_title, NULL, 380, 380) ?>
      </a>
      <?php foreach ($Read->getResult() as $gallery):
        extract($gallery); ?>
        <a href="<?= RAIZ . '/doutor/uploads/' . $gallery_file; ?>" title="<?= $prod_title; ?>" data-fancybox="group1" data-caption="<?=$prod_title?>" class="lightbox">
          <?= Check::Image('doutor/uploads/' . $gallery_file, $prod_title, NULL, 380, 380) ?>
        </a>
        <?php
      endforeach; ?>
    </div>
    <div class="gallery__nav" data-slick='{"slidesToShow": <?=($nImg >= "3") ? "3" : $nImg ?>}'>
      <div class="gallery__item">
        <?=Check::Image('doutor/uploads/' . $prod_cover, $prod_title, NULL, 100, 100);?>
      </div>
      <?php foreach ($Read->getResult() as $gallery):
        extract($gallery); ?>
        <div class="gallery__item">
          <?= Check::Image('doutor/uploads/' . $gallery_file, $prod_title, NULL, 100, 100) ?>
        </div>
        <?php
      endforeach; ?>
    </div>
    <?php
  endif; ?>
</div>
<!-- END CUSTOM GALLERY -->
<?php endif ?>