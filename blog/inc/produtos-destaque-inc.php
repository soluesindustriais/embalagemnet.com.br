<div class="card-group">
<?php	
	$destaquesMPI = [
		array('mpiNum' => '0', 'image' => '01'),
		array('mpiNum' => '1', 'image' => '01'),
		array('mpiNum' => '2', 'image' => '01'),
		array('mpiNum' => '3', 'image' => '01'),
	];

	$wrongIDs = array();
	
	foreach($destaquesMPI as $key => $item):
		$mpi = $vetKey[$item['mpiNum']];
		if($mpi !== null):
			$mpiTitle = $mpi['key'];
			$mpiUrl = $mpi['url'];
			$mpiImage = $item['image']; ?>
			<div class="card card--mpi">
				<a class="card__link" href="<?=$url.$mpiUrl?>" title="<?=$mpiTitle?>">
					<img class="card__image" src="<?=$url?>imagens/informacoes/<?=$mpiUrl."-".$mpiImage?>.jpg" alt="Imagem ilustrativa de <?=$mpiTitle?>" title="<?=$mpiTitle?>" loading="lazy" >
					<h2 class="card__title"><?=$mpiTitle?></h2>
				</a>
			</div>
		<?php else:
			array_push($wrongIDs, $item['mpiNum']);
		endif;		
	endforeach; ?>
</div>

<?php
	if(count($wrongIDs) > 0):
		echo "<script> alert('Os seguintes IDs definidos para os destaques não foram encontrados:\\n\\n".implode(", ", $wrongIDs)."');</script>";
	endif;
?>