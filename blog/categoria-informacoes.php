<?php
include('inc/vetKey.php');
$h1 = 'Categoria Informações';
$title = $h1;
$var = 'Informação';
$desc = 'Categoria Informações - ';
include('inc/head.php');
?>
</head>

<body>
	<? include('inc/topo.php'); ?>
	<main>
		<div class="content" itemscope itemtype="https://schema.org/Products">
			<section>
				<?= $caminho2 ?>
				<div class="wrapper">
					<h1><?= $h1 ?></h1>
					<?php $mpiCatId = "01"; //Exibir cards da categoria ID: XX
						include('inc/mpi-category-inc.php');
						include('inc/social-media.php');
					?>
				</div> <!-- end wrapper --><!-- end container -->
				<? include('inc/aside-mpi-cat.php'); ?>
			</section>
		</div> <!-- end content -->
	</main>
	<? include('inc/footer.php'); ?>
</body>
</html>