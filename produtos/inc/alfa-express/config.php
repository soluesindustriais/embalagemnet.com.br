<?php
$clienteAtivo = "ativo";

//HOMEsolucs
// $tituloCliente recebe ($clienteAtivo igual 'inativo') recebe 'Soluções industriais' se não 'Base Mini Site';
$tituloCliente = ($clienteAtivo == 'inativo') ? 'Soluções industriais' : 'Alfa Express';
$tituloCliente = str_replace(' ', '-', $tituloCliente);

$subTituloCliente = 'Inovando sua indústria com eficiência';
$bannerIndex = 'imagem-banner-fixo';
$minisite = "inc/" . $tituloCliente . "/";
$subdominio = str_replace(' ', '-', $tituloCliente);




//informacoes-vetPalavras
$CategoriaNameInformacoes = "informacoes";


include("$linkminisite" . "conteudos-minisite.php");


// Criar página de produto mpi
$VetPalavrasProdutos = [

    "embalagem-com-ziper-para-alimentos",
    "embalagem-de-polipropileno-para-alimentos",
    "embalagem-de-pao-de-alho",
    "embalagem-laminada-para-alimentos",
    "embalagem-para-amendoim-personalizado",
    "embalagens-bopp-para-picole",
    "embalagens-para-doces-personalizadas",
    "embalagens-personalizadas-para-queijos",
    "fabrica-de-embalagens-para-salgados-congelados",
    "saco-de-polipropileno-para-alimentos"
    


];

//Criar página de Serviço
/* $VetPalavrasInformacoes = [

]; */



// Numero do formulario de cotação
$formCotar = 188;



// Informações Geral.php

$nomeSite = ($clienteAtivo == 'inativo') ? 'Soluções industriais' : 'Alfa Express';
$slogan = ($clienteAtivo == 'inativo') ? "Inovando sua indústria com eficiência" : 'Os melhores no seguimento';
$rua = ($clienteAtivo == 'inativo') ? "Rua Alexandre Dumas" : 'Rua Vereador José Murad, 622';
$bairro = ($clienteAtivo == 'inativo') ? "Santo Amaro" : 'Bairro Esperança';
$cidade = ($clienteAtivo == 'inativo') ? "São Paulo" : 'Três Pontas';
$UF = ($clienteAtivo == 'inativo') ? "SP" : 'MG';
$cep = ($clienteAtivo == 'inativo') ? "CEP: 04717-004" : '37190-000';
$imgLogo = ($clienteAtivo == 'inativo') ? 'logo-site.webp' : 'logo-cliente-2.webp';
$emailContato = ($clienteAtivo == 'inativo') ? '' : 'vendas@alfaexpressembalagens.com.br';
$instagram =  ($clienteAtivo == 'inativo') ? 'https://www.instagram.com/solucoesindustriaisoficial?igsh=MWtvOGF4aXBqODF3MQ==' : '';
$facebook =  ($clienteAtivo == 'inativo') ? 'https://www.facebook.com/solucoesindustriais/' : '';



?>


<!-- VARIAVEIS DE CORES -->
<style>
    :root {
        --cor-pretopantone: #2d2d2f;
        --cor-principaldocliente: #285C63;
        --cor-secundariadocliente: #4c97bb;
        --terceira-cor: #2d2d2f;
        --font-primary: "Poppins";
        --font-secundary: "Poppins";
    }
</style>