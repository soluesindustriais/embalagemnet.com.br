<section class="wrapper flex-wrap-mobile section-content">
    <div class="section-text">
        <h2 class="text-center">Benefícios Chave</h2>
        <ul class="list-benefits d-flex flex-wrap-mobile justify-content-between">
            <li>
                <i class="fa-solid fa-people-arrows"></i>
                <h3>Soluções Personalizadas</h3>
                <p class="text-center font-catamaram"> O Soluções Industriais se destaca na oferta de soluções customizadas, adaptando-se às necessidades específicas de cada cliente no setor industrial. Expertise em criar respostas eficazes para desafios únicos.</p>
            </li>
            <li>
                <i class="fa-solid fa-phone"></i>
                <h3>Atendimento e Suporte Excepcionais</h3>
                <p class="text-center font-catamaram">Com um foco incansável na satisfação do cliente, o Soluções Industriais proporciona suporte técnico de alta qualidade e atendimento ao cliente ágil, garantindo eficiência e resolução rápida de problemas.</p>
            </li>
            <li>
                <i class="fa-solid fa-microchip"></i>
                <h3>Inovação e Tecnologia Avançada</h3>
                <p class="text-center font-catamaram">Líder em inovação, o Soluções Industriais incorpora as mais recentes tecnologias e inovações, assegurando que seus produtos e serviços estejam sempre na vanguarda do setor industrial, oferecendo soluções futuristas.</p>
            </li>
        </ul>
    </div>
</section>