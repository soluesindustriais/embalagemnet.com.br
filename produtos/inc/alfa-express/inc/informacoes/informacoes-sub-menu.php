<?php

foreach ($VetPalavrasInformacoes as $palavra) {
    $palavraSemAcento = strtolower(remove_acentos($palavra));
    $palavraSemHifenUpperCase = ucwords(str_replace("-", " ", $palavra));

    echo "<li class=\"dropdown\" ><a class=\"link-sub-list first-child\" href=\"" . $linkdominio . $palavraSemAcento . "\" title=\"$palavraSemHifenUpperCase\">$palavraSemHifenUpperCase</a></li>\n";
}

foreach ($VetPalavrasProdutos as $produtos) {
    $palavraSemAcento = strtolower(remove_acentos($produtos));
    $palavraSemHifenUpperCase = ucwords(str_replace("-", " ", $produtos));

    echo "<li class=\"dropdown\" ><a class=\"link-sub-list first-child\" href=\"" . $linkdominio . $palavraSemAcento . "\" title=\"$palavraSemHifenUpperCase\">$palavraSemHifenUpperCase</a></li>\n";
}
